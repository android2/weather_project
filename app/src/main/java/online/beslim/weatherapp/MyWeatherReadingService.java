package online.beslim.weatherapp;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

public class MyWeatherReadingService extends IntentService {

    private static final String ACTION_GET_WEATHER = "online.beslim.weatherapp.action.GET_WEATHER";
    private static final String EXTRA_PARAM1 = "online.beslim.weatherapp.extra.CITY";

    public MyWeatherReadingService() {
        super("MyWeatherReadingService");
    }

    public static void startActionGetWeather(Context context, String param1) {
        Intent intent = new Intent(context, MyWeatherReadingService.class);
        intent.setAction(ACTION_GET_WEATHER);
        intent.putExtra(EXTRA_PARAM1, param1);
        context.startService(intent);
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_GET_WEATHER.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                handleActionGetWeather(param1);
            }
        }
    }

    private void handleActionGetWeather(String param1) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

}
