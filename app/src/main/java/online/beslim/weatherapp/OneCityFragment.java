package online.beslim.weatherapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OneCityFragment extends Fragment {

    public static final String CITY_ID = "city_name";
    private TextView cityNameLabel;
    private ImageView cityImage;
    private OneCity city;
    private OpenWeather openWeather;
    private TextView cityTemp;
    private TextView cityTempMin;
    private TextView cityTempMax;
    private TextView cityPressure;
    private TextView cityHumidity;
    private EditText cityRussian;
    private EditText cityEnglish;
    private EditText cityImageURL;
    private Button citySaveButton;
    private Context context;

    private WorkWithDB workWithDB;

    public OneCityFragment() {
    }

    private View.OnClickListener citySaveButtonListener = v -> {
        String cit = cityRussian.getText().toString();
        String cityEn = cityEnglish.getText().toString();
        String cityIm = cityImageURL.getText().toString();
        if (cit.isEmpty()) return;

        SQLiteDatabase db = workWithDB.getWritableDatabase();
        Cursor c = db.query(WorkWithDB.TABLE_CITIES, new String[]{WorkWithDB.COLUMN_CITY_NAME}, WorkWithDB.COLUMN_CITY_NAME + " = ?", new String[]{cit}, null, null, null);

        boolean cityInDB = c.moveToFirst();
        c.close();

        ContentValues cv = new ContentValues();
        if (cityInDB) {
            cv.put(WorkWithDB.COLUMN_CITY_NAME_ENG, cityEn);
            cv.put(WorkWithDB.COLUMN_CITY_IMG, cityIm);
            db.update(WorkWithDB.TABLE_CITIES, cv, WorkWithDB.COLUMN_CITY_NAME + " = ?", new String[]{cit});
        } else {
            cv.put(WorkWithDB.COLUMN_CITY_NAME, cit);
            cv.put(WorkWithDB.COLUMN_CITY_NAME_ENG, cityEn);
            cv.put(WorkWithDB.COLUMN_CITY_IMG, cityIm);
            cv.put(WorkWithDB.COLUMN_CITY_TEMPERATURE, 0);
            cv.put(WorkWithDB.COLUMN_CITY_TEMP_MIN, 0);
            cv.put(WorkWithDB.COLUMN_CITY_TEMP_MAX, 0);
            cv.put(WorkWithDB.COLUMN_CITY_PRESSURE, 0);
            cv.put(WorkWithDB.COLUMN_CITY_HUMIDITY, 0);
            db.insert(WorkWithDB.TABLE_CITIES, null, cv);

        }
        db.close();

        OneCity city = OneCityFragment.this.city;
        city.setName(cit);
        city.setNameEng(cityEn);
        city.setImgUrl(cityIm);

        cityNameLabel.setText(cit);
        downloadImageAndWeather();


    };

    private void write_weather(String cit, double tem, double tem_min, double tem_max, double press, double humid) {
        SQLiteDatabase db = workWithDB.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(WorkWithDB.COLUMN_CITY_TEMPERATURE, tem);
        cv.put(WorkWithDB.COLUMN_CITY_TEMP_MIN, tem_min);
        cv.put(WorkWithDB.COLUMN_CITY_TEMP_MAX, tem_max);
        cv.put(WorkWithDB.COLUMN_CITY_PRESSURE, press);
        cv.put(WorkWithDB.COLUMN_CITY_HUMIDITY, humid);
        db.update(WorkWithDB.TABLE_CITIES, cv, WorkWithDB.COLUMN_CITY_NAME + " = ?", new String[]{cit});

        db.close();
    }

    @Override
    public void onStart() {
        super.onStart();
        cityRussian.setText(city.getName());
        cityEnglish.setText(city.getNameEng());
        cityImageURL.setText(city.getImgUrl());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_one_city, container, false);
        cityNameLabel = view.findViewById(R.id.fragment_one_city_label);
        cityImage = view.findViewById(R.id.fragment_one_city_image);
        cityNameLabel.setText(city.getName());
        cityImage.setImageResource(App.getImage(city.getName()));
        cityTemp = view.findViewById(R.id.fragment_one_city_temp);
        cityTempMin = view.findViewById(R.id.fragment_one_city_temp_min);
        cityTempMax = view.findViewById(R.id.fragment_one_city_temp_max);
        cityPressure = view.findViewById(R.id.fragment_one_city_pressure);
        cityHumidity = view.findViewById(R.id.fragment_one_city_humidity);
        cityRussian = view.findViewById(R.id.fragment_one_city_city_russian);
        cityEnglish = view.findViewById(R.id.fragment_one_city_city_english);
        cityImageURL = view.findViewById(R.id.fragment_one_city_image_url);
        citySaveButton = view.findViewById(R.id.fragment_one_city_save_button);
        citySaveButton.setOnClickListener(citySaveButtonListener);


        context = view.getContext();


        cityRussian.setText(city.getName());
        cityEnglish.setText(city.getNameEng());
        cityImageURL.setText(city.getImgUrl());

        cityTemp.setText(String.format("Температура: %.2f ºC", city.getTemperature()));
        cityTempMin.setText(String.format("Минимум: %.2f ºC", city.getTemp_min()));
        cityTempMax.setText(String.format("Максимум: %.2f ºC", city.getTemp_max()));
        cityPressure.setText("Давление: " + (int) city.getPressure() + " мбар");
        cityHumidity.setText("Влажность: " + (int) city.getHumidity() + "%");


        workWithDB = new WorkWithDB(getContext().getApplicationContext());

        initDownloadingInstrument();

        downloadImageAndWeather();


        return view;
    }

    private void downloadImageAndWeather() {
        requestWeather(city.getNameEng(), "fd12c934107dbfe623639323b30e7e5e");

        if (!city.getImgUrl().isEmpty()) {
            Picasso.with(context).load(city.getImgUrl()).transform(new CircleTransformation()).into(cityImage);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Bundle bundle = getArguments();
        if (bundle != null) {
            city = (OneCity) bundle.getSerializable(CITY_ID);
        }
    }

    public void updateCity(OneCity city) {
        this.city = city;
        cityNameLabel.setText(city.getName());
        cityImage.setImageResource(App.getImage(city.getName()));
    }

    private void initDownloadingInstrument() {
        Retrofit retrofit;
        retrofit = new Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org/") //Базовая часть адреса
                .addConverterFactory(GsonConverterFactory.create()) //Конвертер, необходимый для преобразования JSON'а в объекты
                .build();
        openWeather = retrofit.create(OpenWeather.class); //Создаем объект, при помощи которого будем выполнять запросы
    }

    private void showReqError() {
        cityTemp.setText("Температура: ");
        cityTempMin.setText("Минимум: ошибка");
        cityTempMax.setText("Максимум: ошибка");
        cityPressure.setText("Давление: ошибка");
        cityHumidity.setText("Влажность: ошибка");

    }

    private void requestWeather(String city, String keyApi) {
        openWeather.loadWeather(city, keyApi)
                .enqueue(new Callback<WeatherRequest>() {
                    @Override
                    public void onResponse(Call<WeatherRequest> call, Response<WeatherRequest> response) {
                        Main mn;
                        if (response.body() != null && ((mn = response.body().getMain()) != null)) {
                            cityTemp.setText(String.format("Температура: %.2f ºC", mn.getTemp() - 273.15));
                            cityTempMin.setText(String.format("Минимум: %.2f ºC", mn.getTemp_min() - 273.15));
                            cityTempMax.setText(String.format("Максимум: %.2f ºC", mn.getTemp_max() - 273.15));
                            cityPressure.setText("Давление: " + mn.getPressure() + " мбар");
                            cityHumidity.setText("Влажность: " + mn.getHumidity() + "%");
                            write_weather(OneCityFragment.this.city.getName(), mn.getTemp() - 273.15, mn.getTemp_min() - 273.15,
                                    mn.getTemp_max() - 273.15, mn.getPressure(), mn.getHumidity());
                        } else showReqError();
                    }

                    @Override
                    public void onFailure(Call<WeatherRequest> call, Throwable t) {
                        showReqError();
                    }
                });

    }
}
