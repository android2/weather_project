package online.beslim.weatherapp;


import android.os.Handler;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class OkHttpRequester {
    BrowserFragment.OnResponseCompleted listener;
    Handler handler;

    public OkHttpRequester(BrowserFragment.OnResponseCompleted listener, Handler handler) {
        this.listener = listener;
        this.handler = handler;
    }

    public void run(String url) {
        OkHttpClient client = new OkHttpClient();
        Request.Builder builder = new Request.Builder();

        Request request;
        try {
            builder.url(url);
            request = builder.build();
        } catch (Exception e) {
            return;
        }

        Call call = client.newCall(request);
        call.enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                handler.post(() -> listener.onComplete("Не могу открыть страницу."));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String answer;
                if (response.body() != null) {
                    answer = response.body().string();
                } else {
                    answer = "";
                }
                handler.post(() -> listener.onComplete(answer));
            }
        });

    }


}
