package online.beslim.weatherapp;

import android.app.Application;

/**
 *
 */
public class App extends Application {

    private static App instance;
    private CityUtils utils;

    public static int getImage(String name) {
        return instance.utils.getImage(name);
    }

    public static App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        utils = new CityUtils(instance.getResources());
    }
}
