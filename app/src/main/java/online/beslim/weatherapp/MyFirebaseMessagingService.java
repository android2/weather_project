package online.beslim.weatherapp;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    public MyFirebaseMessagingService() {


    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);

        Log.i("MyFirebase", "Новый токен: " + s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);


        if (remoteMessage.getNotification() != null && MainActivity.pushShower != null) {
            MainActivity.pushShower.showPush(remoteMessage.getNotification().getBody());
        }


    }

}
