package online.beslim.weatherapp;

import android.os.AsyncTask;

public class Requester extends AsyncTask<Integer, Integer, Double> {
    MainActivity.MyAsyncListener listener;

    public Requester(MainActivity.MyAsyncListener listener) {
        this.listener = listener;
    }

    @Override
    protected Double doInBackground(Integer... integers) {
        int HiLim = integers[0];
        double r = 1;
        for (int i = 0; i < HiLim; i++) {
            r = i * 0.01 + r / 1.789;

            if ((i % 10000) == 0) publishProgress(i);
        }
        return r;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        listener.onProgress(values[0]);
    }

    @Override
    protected void onPostExecute(Double aDouble) {
        //super.onPostExecute(aDouble);
        listener.onFinishCalculations(aDouble);
    }
}
