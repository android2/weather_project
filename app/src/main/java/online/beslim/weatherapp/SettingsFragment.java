package online.beslim.weatherapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;


public class SettingsFragment extends Fragment {
    private EditText editTextCity;
    private EditText editTextHomePage;
    private EditText editTextLastPage;
    private RadioButton radioButtonFromHome;
    private RadioButton radioButtonFromLast;
    private SharedPreferences sharedPreferences;

    public SettingsFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        initUi(view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        loadSettings();
    }

    @Override
    public void onStop() {
        super.onStop();
        saveSettings();
    }

    private void saveSettings() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("CITY", editTextCity.getText().toString());
        editor.putString("HOME_PAGE", editTextHomePage.getText().toString());
        editor.putString("LAST_PAGE", editTextLastPage.getText().toString());
        editor.putBoolean("FROM_HOME", radioButtonFromHome.isChecked());
        editor.apply();
    }

    private void initUi(View view) {

        editTextCity = view.findViewById(R.id.fragment_settings_city);
        editTextHomePage = view.findViewById(R.id.fragment_settings_home);
        editTextLastPage = view.findViewById(R.id.fragment_settings_last);
        radioButtonFromHome = view.findViewById(R.id.fragment_settings_from_home);
        radioButtonFromLast = view.findViewById(R.id.fragment_settings_from_last);


    }

    private void loadSettings() {
        sharedPreferences = getActivity().getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        editTextCity.setText(sharedPreferences.getString("CITY", "Уфа"));
        editTextHomePage.setText(sharedPreferences.getString("HOME_PAGE", "http://www.google.ru"));
        editTextLastPage.setText(sharedPreferences.getString("LAST_PAGE", "http://www.google.ru"));
        boolean fromeHome = sharedPreferences.getBoolean("FROM_HOME", true);
        if (fromeHome) radioButtonFromHome.setChecked(true);
        else radioButtonFromLast.setChecked(true);
    }


}
