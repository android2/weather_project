package online.beslim.weatherapp;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static SmsShower smsShower;
    public static PushShower pushShower;

    public static final String TAG = "MainActivity";
    public static final int CALC_ITERATIONS = 100000000;
    private static final int SMS_PERMISSION_CODE = 0;
    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS = 1;
    public static final String CITY_KEY = "MyCity";
    private final List<OneCity> allCity;
    private AllCityFragment allCityFragment;
    private OneCityFragment oneCityFragment;
    private BrowserFragment browserFragment;
    private SettingsFragment settingsFragment;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private CustomView sensorsPanel;
    private SensorManager mSensorManager;
    private Sensor mHumiditySensor;
    private Sensor mTemperatureSensor;
    private Button startButton;
    private ProgressBar calculationProgress;
    private TextView recSms;
    private EditText smsText;
    private EditText smsPhone;
    private TextView pushMsg;


    private boolean isHumiditySensorPresent;
    private boolean isTemperatureSensorPresent;
    private float mLastKnownRelativeHumidity = 0;

    private WorkWithDB workWithDB;


    private MyAsyncListener asyncListener = new MyAsyncListener() {
        @Override
        public void onProgress(int progress) {
            calculationProgress.setProgress(progress);
        }

        @Override
        public void onFinishCalculations(double result) {
            sensorsPanel.setCalcResult("результат: " + result);
            startButton.setEnabled(true);
        }
    };
    private Button smsSend;

    private void initUi() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(v -> replaceNewWithOneCityFragment());


        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(menuItem -> {
            int id = menuItem.getItemId();

            if (id == R.id.nav_settings) {
                replaceWithSettingsFragment();
            } else if (id == R.id.nav_browser) {
                replaceWithBrowserFragment();
            }

            drawer.closeDrawer(GravityCompat.START);
            return true;
        });

        sensorsPanel = findViewById(R.id.main_activity_sensors_data);
        sensorsPanel.setVisibility(View.GONE);
        startButton = findViewById(R.id.main_activity_start);
        calculationProgress = findViewById(R.id.main_activity_progressBar);
        calculationProgress.setMax(CALC_ITERATIONS);

        startButton.setOnClickListener(v -> {
            startButton.setEnabled(false);
            calculationProgress.setProgress(0);
            sensorsPanel.setCalcResult("секундочку...");
            new Requester(asyncListener).execute(CALC_ITERATIONS);

        });

        recSms = findViewById(R.id.main_activity_received_sms);

        allCityFragment = new AllCityFragment();
        oneCityFragment = new OneCityFragment();
        browserFragment = new BrowserFragment();
        settingsFragment = new SettingsFragment();

        smsText = findViewById(R.id.main_activity_sms);
        smsPhone = findViewById(R.id.main_activity_number);
        smsSend = findViewById(R.id.main_activity_send);

        smsSend.setOnClickListener(v -> {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(smsPhone.getText().toString(), null, smsText.getText().toString(), null, null);


        });

        pushMsg = findViewById(R.id.main_activity_push_msg);

        replaceWithAllCityFragment();


    }

    private SensorEventListener listener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            if (event.sensor.getType() == Sensor.TYPE_RELATIVE_HUMIDITY) {
                mLastKnownRelativeHumidity = event.values[0];
                sensorsPanel.setRelativeHumidityCapt("отн. влажность " + mLastKnownRelativeHumidity + "%");

            } else if (event.sensor.getType() == Sensor.TYPE_AMBIENT_TEMPERATURE) {
                if (mLastKnownRelativeHumidity != 0) {
                    float temperature = event.values[0];
                    float absoluteHumidity = calculateAbsoluteHumidity(temperature, mLastKnownRelativeHumidity);
                    sensorsPanel.setTempCapt("температура " + temperature + " гр.С");
                    sensorsPanel.setAbsoluteHumidityCapt("абс. влажность " + absoluteHumidity + " г/м3");
                }
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    public MainActivity() {
        allCity = new ArrayList<>();
    }

    public float calculateAbsoluteHumidity(float temperature, float relativeHumidity) {
        float Dv = 0;
        float m = 17.62f;
        float Tn = 243.12f;
        float Ta = 216.7f;
        float Rh = relativeHumidity;
        float Tc = temperature;
        float A = 6.112f;
        float K = 273.15f;

        Dv = (float) (Ta * (Rh / 100) * A * Math.exp(m * Tc / (Tn + Tc)) / (K + Tc));

        return Dv;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        initSensors();

        workWithDB = new WorkWithDB(App.getInstance());


        initCityList();
        initUi();
        if (!hasReadSmsPermission()) {
            showRequestPermissionsInfoAlertDialog();
        }

        checkForSmsPermission();

        smsShower = message -> recSms.setText(message);
        pushShower = message -> pushMsg.setText(message);
    }

    private boolean hasReadSmsPermission() {
        return ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED;
    }

    private void showRequestPermissionsInfoAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Разрешение на SMS");
        builder.setMessage("Можно принимать и отправлять SMS?");
        builder.setPositiveButton("OK", (dialog, which) -> {
            dialog.dismiss();
            requestReadAndSendSmsPermission();
        });
        builder.show();
    }

    private void requestReadAndSendSmsPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.READ_SMS)) {
            return;
        }
        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS},
                SMS_PERMISSION_CODE);
    }

    private void checkForSmsPermission() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.SEND_SMS},
                    MY_PERMISSIONS_REQUEST_SEND_SMS);
        } else {
            smsSend.setEnabled(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_SEND_SMS: {
                if (permissions[0].equalsIgnoreCase
                        (Manifest.permission.SEND_SMS)
                        && grantResults[0] ==
                        PackageManager.PERMISSION_GRANTED) {
                    // Permission was granted. Enable sms button.
                    smsSend.setEnabled(true);
                } else {
                    smsSend.setEnabled(false);
                }
            }
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isHumiditySensorPresent || isTemperatureSensorPresent) {
            mSensorManager.unregisterListener(listener);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isHumiditySensorPresent) {
            mSensorManager.registerListener(listener, mHumiditySensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
        if (isTemperatureSensorPresent) {
            mSensorManager.registerListener(listener, mTemperatureSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSensorManager = null;
        mHumiditySensor = null;
        mTemperatureSensor = null;
        smsShower = null;
        pushShower = null;
    }

    private void initSensors() {
        mSensorManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
        if (mSensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY) != null) {
            mHumiditySensor = mSensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
            isHumiditySensorPresent = true;
        } else {
            isHumiditySensorPresent = false;
        }

        if (mSensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE) != null) {
            mTemperatureSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
            isTemperatureSensorPresent = true;
        } else {
            isTemperatureSensorPresent = false;
        }


    }

    private void initCityList() {
        allCity.clear();

        SQLiteDatabase db = workWithDB.getWritableDatabase();
        Cursor c = db.query(WorkWithDB.TABLE_CITIES,
                new String[]{WorkWithDB.COLUMN_CITY_NAME, WorkWithDB.COLUMN_CITY_NAME_ENG, WorkWithDB.COLUMN_CITY_IMG,
                        WorkWithDB.COLUMN_CITY_TEMPERATURE, WorkWithDB.COLUMN_CITY_TEMP_MIN, WorkWithDB.COLUMN_CITY_TEMP_MAX,
                        WorkWithDB.COLUMN_CITY_PRESSURE, WorkWithDB.COLUMN_CITY_HUMIDITY},
                null, null, null, null, WorkWithDB.COLUMN_CITY_NAME);

        if (c.moveToFirst()) {
            do {
                allCity.add(new OneCity(c.getString(0), c.getString(0), c.getString(1),
                        c.getString(2), c.getDouble(3), c.getDouble(4), c.getDouble(5),
                        c.getDouble(6), c.getDouble(7)));

            } while (c.moveToNext());

        }
        c.close();
        db.close();
    }

    public interface MyAsyncListener {
        void onProgress(int progress);

        void onFinishCalculations(double result);
    }

    private void replaceNewWithOneCityFragment() {
        Bundle args = new Bundle();
        args.putSerializable(OneCityFragment.CITY_ID, new OneCity("", "", "", "", 0, 0, 0, 0, 0));
        oneCityFragment.setArguments(args);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_activity_container, oneCityFragment)
                .commit();
    }

    private void replaceWithAllCityFragment() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AllCityFragment.CITY_LIST, (Serializable) allCity);
        allCityFragment.setArguments(bundle);
        allCityFragment.setListener(this::replaceWithOneCityFragment);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_activity_container, allCityFragment)
                .commit();
    }
    private void replaceWithOneCityFragment(int id) {
        Bundle args = new Bundle();
        args.putSerializable(OneCityFragment.CITY_ID, allCity.get(id));
        oneCityFragment.setArguments(args);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_activity_container, oneCityFragment)
                .commit();
    }

    private void replaceWithBrowserFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_activity_container, browserFragment)
                .commit();
    }

    private void replaceWithSettingsFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_activity_container, settingsFragment)
                .commit();
    }


    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (oneCityFragment.isAdded() || browserFragment.isAdded() || settingsFragment.isAdded()) {
                initCityList();
                replaceWithAllCityFragment();
                return;
            }
            super.onBackPressed();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_about) {
            Toast.makeText(this, "Погодное приложение", Toast.LENGTH_LONG).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
