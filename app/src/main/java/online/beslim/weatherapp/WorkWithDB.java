package online.beslim.weatherapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class WorkWithDB extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String TABLE_CITIES = "cities";
    public static final String COLUMN_CITY_NAME = "city_name";
    public static final String COLUMN_CITY_NAME_ENG = "city_name_eng";
    public static final String COLUMN_CITY_IMG = "city_img";
    public static final String COLUMN_CITY_TEMPERATURE = "temperature";
    public static final String COLUMN_CITY_TEMP_MIN = "temp_min";
    public static final String COLUMN_CITY_TEMP_MAX = "temp_max";
    public static final String COLUMN_CITY_PRESSURE = "pressure";
    public static final String COLUMN_CITY_HUMIDITY = "humidity";
    private static final String DATABASE_NAME = "city_measures.db";


    public WorkWithDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " +
                TABLE_CITIES +
                " (" + COLUMN_CITY_NAME + " TEXT PRIMARY KEY," +
                COLUMN_CITY_NAME_ENG + " TEXT," +
                COLUMN_CITY_IMG + " TEXT," +
                COLUMN_CITY_TEMPERATURE + " REAL," +
                COLUMN_CITY_TEMP_MIN + " REAL," +
                COLUMN_CITY_TEMP_MAX + " REAL," +
                COLUMN_CITY_PRESSURE + " REAL," +
                COLUMN_CITY_HUMIDITY + " REAL);"
        );

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
