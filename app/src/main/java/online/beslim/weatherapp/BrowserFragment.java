package online.beslim.weatherapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;


public class BrowserFragment extends Fragment {

    private EditText editTextUri;
    private Button navButton;
    private ImageButton navHome;
    private WebView webView;
    private SharedPreferences sharedPreferences;


    OnResponseCompleted listener = new OnResponseCompleted() {
        @Override
        public void onComplete(String content) {
            if (content != null) webView.loadDataWithBaseURL("", content, "text/html", "UTF-8", "");
        }
    };

    OkHttpRequester okHttpRequester;

    public BrowserFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        okHttpRequester = new OkHttpRequester(listener, new Handler());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_browser, container, false);
        editTextUri = view.findViewById(R.id.browser_uri);
        navButton = view.findViewById(R.id.browser_navigate);
        navButton.setOnClickListener(v -> {
            String uri = editTextUri.getText().toString();
            okHttpRequester.run(uri);
        });

        navHome = view.findViewById(R.id.browser_home);
        navHome.setOnClickListener(v -> {
                    editTextUri.setText(sharedPreferences.getString("HOME_PAGE", "http://www.google.ru"));
                    okHttpRequester.run(editTextUri.getText().toString());

                }
        );

        webView = view.findViewById(R.id.browser_browser);
        webView.getSettings().setJavaScriptEnabled(true);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        loadSettings();
        String uri = editTextUri.getText().toString();
        okHttpRequester.run(uri);
    }

    @Override
    public void onStop() {
        super.onStop();
        saveSettings();
    }

    private void saveSettings() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("LAST_PAGE", editTextUri.getText().toString());
        editor.apply();

    }

    private void loadSettings() {
        sharedPreferences = getActivity().getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        editTextUri.setText(sharedPreferences.getString(
                sharedPreferences.getBoolean("FROM_HOME", true) ?
                        "HOME_PAGE" : "LAST_PAGE", "http://www.google.ru"));
    }

    interface OnResponseCompleted {
        void onComplete(String content);
    }


}
