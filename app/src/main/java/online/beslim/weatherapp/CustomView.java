package online.beslim.weatherapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class CustomView extends View {

    private static final String TAG = "CustomTagZes";
    private Paint paintText;
    private int textColor;

    private String tempCapt;
    private String absoluteHumidityCapt;
    private String relativeHumidityCapt;

    private String calcResult;

    public String getCalcResult() {
        return calcResult;
    }

    public void setCalcResult(String calcResult) {
        this.calcResult = calcResult;
    }

    public CustomView(Context context) {
        super(context);
        initAttrs(null);
        initUi(context);
    }

    public CustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttrs(attrs);
        initUi(context);
    }

    public CustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttrs(attrs);
        initUi(context);
    }

    public String getTempCapt() {
        return tempCapt;
    }

    public void setTempCapt(String tempCapt) {
        this.tempCapt = tempCapt;
        invalidate();
    }

    public String getAbsoluteHumidityCapt() {
        return absoluteHumidityCapt;
    }

    public void setAbsoluteHumidityCapt(String absoluteHumidityCapt) {
        this.absoluteHumidityCapt = absoluteHumidityCapt;
        invalidate();

    }

    public String getRelativeHumidityCapt() {
        return relativeHumidityCapt;
    }

    public void setRelativeHumidityCapt(String relativeHumidityCapt) {
        this.relativeHumidityCapt = relativeHumidityCapt;
        invalidate();

    }

    private void initAttrs(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CustomView, 0, 0);
            textColor = typedArray.getResourceId(R.styleable.CustomView_text_color, android.R.color.holo_red_dark);
//            isImageBlack = typedArray.getBoolean(R.styleable.CustomView_is_image_black, false);
            typedArray.recycle();
        } else {
            textColor = android.R.color.holo_red_dark;
//            isImageBlack = false;
        }
    }

    private void initUi(Context context) {
        paintText = new Paint();
        paintText.setTextSize(40);
        paintText.setColor(context.getResources().getColor(textColor));
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public void layout(int l, int t, int r, int b) {
        super.layout(l, t, r, b);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (tempCapt != null) canvas.drawText(tempCapt, 10, 50, paintText);
        if (absoluteHumidityCapt != null) canvas.drawText(absoluteHumidityCapt, 10, 90, paintText);
        if (relativeHumidityCapt != null) canvas.drawText(relativeHumidityCapt, 10, 130, paintText);
        if (calcResult != null) canvas.drawText(calcResult, 10, 170, paintText);

    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        if (action == MotionEvent.ACTION_DOWN) {
            invalidate();
        } else if (action == MotionEvent.ACTION_UP) {
            invalidate();
        }
        return true;
    }
}
