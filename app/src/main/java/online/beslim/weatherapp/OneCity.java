package online.beslim.weatherapp;

import java.io.Serializable;

public class OneCity implements Serializable {

    private String name;
    private String nameEng;
    private String imgUrl;
    private double temperature;
    private double temp_min;
    private double temp_max;
    private double pressure;
    private double humidity;

    OneCity(String name, String description, String nameEng, String imgUrl, double temperature, double temp_min,
            double temp_max, double pressure, double humidity) {
        this.name = name;
        this.description = description;
        this.nameEng = nameEng;
        this.imgUrl = imgUrl;
        this.temperature = temperature;
        this.temp_min = temp_min;
        this.temp_max = temp_max;
        this.pressure = pressure;
        this.humidity = humidity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameEng() {
        return nameEng;
    }

    public void setNameEng(String nameEng) {
        this.nameEng = nameEng;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getTemp_min() {
        return temp_min;
    }

    public void setTemp_min(double temp_min) {
        this.temp_min = temp_min;
    }

    public double getTemp_max() {
        return temp_max;
    }

    public void setTemp_max(double temp_max) {
        this.temp_max = temp_max;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    private final String description;

    public String getDescription() {
        return description;
    }


}
